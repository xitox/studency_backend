import { Module } from '@nestjs/common';
import { ImportCsvController } from './import-csv.controller';

@Module({
  controllers: [ImportCsvController],
})
export class ImportCsvModule {}
