/* eslint-disable prettier/prettier */

import { Controller, UploadedFile, Post, UseInterceptors } from '@nestjs/common';
import * as fs from "fs";
import * as path from "path";
import { parse } from 'csv-parse';
import { Etudiant } from 'src/components/etudiant/etudiant.entity';
import { Groupe } from 'src/components/groupe/groupe.entity';
import { getRepository, Repository } from 'typeorm';
import { extname } from 'path';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

type etudiantCSV = {
  numEtudiant : number
  nom : string
  prenom : string
  groupe : number
  annee : number
};


@Controller('import-csv')
export class ImportCsvController {

  listeEtudiant : Etudiant[] = [];

    @Post("/upload")
    @UseInterceptors(FileInterceptor("csv", {
      storage: diskStorage({
        destination: "./csv",
        filename: (req, file, cb) => {
          const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
          cb(null, `${randomName}${extname(file.originalname)}`)
        }
      })
    }))
    uploadCSV(@UploadedFile() file) {
      this.loadCsvStudent(file)
    }
   
    async createStudent(newEtudiant : etudiantCSV) {
      const etudiant = new Etudiant();
      etudiant.numero   = newEtudiant.numEtudiant;
      etudiant.nom      = newEtudiant.nom;
      etudiant.prenom   = newEtudiant.prenom;
      etudiant.password = newEtudiant.nom.substring(0,Math.round(Math.min(newEtudiant.nom.length/3,3)));
      this.listeEtudiant.push(etudiant);
      return await getRepository(Etudiant).save(etudiant);
    }

    async createGroupe(groupeNumero : number, annee : number) {
      const groupe = new Groupe();
      groupe.numero = groupeNumero;
      groupe.annee  = annee;
      return await getRepository(Groupe).save(groupe);
    }

    loadCsvStudent(file : any) : Etudiant[]{
        //const csvFilePath = path.resolve( 'eleves.csv');
        console.log(file)
        const csvFilePath = process.cwd() + '/' + file.path;
      
        const headers = ['numEtudiant', 'nom', 'prenom', 'groupe','annee'];
      
        const fileContent = fs.readFileSync(csvFilePath, { encoding: 'utf-8' });
      
        parse(fileContent, {
          delimiter: ';',
          columns: headers,
          from_line: 2
        }, (error, result: etudiantCSV[]) => {
          if (error) {
            console.error(error);
          }
          result.forEach( etudiant => this.createStudent(etudiant))
        });
        return this.listeEtudiant;
    }

}
