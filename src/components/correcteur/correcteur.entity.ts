import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Lot } from '../lot/lot.entity';

@Entity()
export class Correcteur {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userName: string;

  @Column()
  password: string;

  @OneToMany(() => Lot, (lot) => lot.correcteur)
  lots: Lot[];
}
