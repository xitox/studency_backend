import { Body, Controller, HttpStatus, Post, Res } from '@nestjs/common';
import { LoginReq } from './interfaces/LoginReq.dto';
import { CorrecteurService } from './correcteur.service';
import * as bcrypt from 'bcrypt';
import { Response } from 'express';
import { LoginRes } from './interfaces/LoginRes.dto';

@Controller('teacher')
export class CorrecteurController {
  constructor(private service: CorrecteurService) {}

  @Post('login')
  async connect(@Body() loginReq: LoginReq, @Res() response: Response) {
    let userFound: any;
    try {
      userFound = await this.service.findOneByUserName(loginReq.userid);
    } catch (error) {
      response.status(HttpStatus.FORBIDDEN).send();
      return;
    }
    const hashedPassword = userFound.password;
    const userid = userFound.userName;
    bcrypt
      .compare(loginReq.password, hashedPassword)
      .then((validPassword) => {
        if (validPassword) {
          const user: LoginRes = { userid: userid };
          response.status(HttpStatus.ACCEPTED).send(user);
          return;
        } else {
          response.status(HttpStatus.FORBIDDEN).send();
          return;
        }
      })
      .catch((err) => console.log('error: ' + err));
  }
}
