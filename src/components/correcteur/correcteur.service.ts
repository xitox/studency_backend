import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Correcteur } from './correcteur.entity';

@Injectable()
export class CorrecteurService {
  constructor(
    @InjectRepository(Correcteur)
    private CorrecteurRepository: Repository<Correcteur>,
  ) {}

  findAll(): Promise<Correcteur[]> {
    return this.CorrecteurRepository.find();
  }

  findOneByUserName(userName: string): Promise<Correcteur> {
    return this.CorrecteurRepository.findOneOrFail({
      where: { userName: userName },
    });
  }

  async remove(id: string): Promise<void> {
    await this.CorrecteurRepository.delete(id);
  }
}
