import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CorrecteurService } from './correcteur.service';
import { Correcteur } from './correcteur.entity';
import { CorrecteurController } from './correcteur.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Correcteur])],
  providers: [CorrecteurService],
  controllers: [CorrecteurController],
})
export class CorrecteurModule {}
