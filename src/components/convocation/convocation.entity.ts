import {
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { Etudiant } from '../etudiant/etudiant.entity';
import { Vague } from '../vague/vague.entity';

@Entity()
export class Convocation {
  @PrimaryGeneratedColumn()
  numero: number;

  @ManyToOne(() => Etudiant)
  @JoinColumn()
  etudiant: Etudiant;

  @ManyToOne(() => Vague, (vague) => vague.numero)
  vague: Vague;
}
