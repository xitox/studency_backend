import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Convocation } from './convocation.entity';

@Injectable()
export class ConvocationService {
  constructor(
    @InjectRepository(Convocation)
    private convocationRepository: Repository<Convocation>,
  ) {}

  findAll(): Promise<Convocation[]> {
    return this.convocationRepository.find();
  }

  findOne(id: string): Promise<Convocation> {
    return this.convocationRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.convocationRepository.delete(id);
  }
}
