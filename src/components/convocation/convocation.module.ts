import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConvocationService } from './convocation.service';
import { ConvocationController } from './convocation.controller';
import { Convocation } from './convocation.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Convocation])],
  providers: [ConvocationService],
  controllers: [ConvocationController],
})
export class ConvocationModule {}
