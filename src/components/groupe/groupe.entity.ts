import { Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class Groupe {
  @PrimaryColumn()
  numero: number;

  @PrimaryColumn()
  annee: number;
}
