import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Groupe } from './groupe.entity';

@Injectable()
export class GroupeService {
  constructor(
    @InjectRepository(Groupe)
    private groupeRepository: Repository<Groupe>,
  ) {}

  findAll(): Promise<Groupe[]> {
    return this.groupeRepository.find();
  }

  findOne(id: string): Promise<Groupe> {
    return this.groupeRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.groupeRepository.delete(id);
  }
}
