import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GroupeService } from './groupe.service';
import { GroupeController } from './groupe.controller';
import { Groupe } from './groupe.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Groupe])],
  providers: [GroupeService],
  controllers: [GroupeController],
})
export class GroupeModule {}
