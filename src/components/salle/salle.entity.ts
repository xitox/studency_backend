import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Salle {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  lieu: string;
  @Column()
  etage: number;
  @Column()
  batiment: string;
  @Column()
  numero: number;
  @Column()
  capacitePc: number;
}
