import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Salle } from './salle.entity';

@Injectable()
export class SalleService {
  constructor(
    @InjectRepository(Salle)
    private salleRepository: Repository<Salle>,
  ) {}

  findAll(): Promise<Salle[]> {
    return this.salleRepository.find();
  }

  findOne(id: string): Promise<Salle> {
    return this.salleRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.salleRepository.delete(id);
  }
}
