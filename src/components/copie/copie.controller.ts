import { Controller, Get, Param } from '@nestjs/common';
import { DomaineService } from '../domaine/domaine.service';
import { Copie } from './copie.entity';
import { CopieService } from './copie.service';

@Controller('copie')
export class CopieController {
  constructor(private service: CopieService) {}

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Copie> {
    return this.service.findOne(id);
  }
}
