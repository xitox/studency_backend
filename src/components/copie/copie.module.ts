import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CopieService } from './copie.service';
import { Copie } from './copie.entity';
import { CopieController } from './copie.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Copie])],
  providers: [CopieService],
  controllers: [CopieController],
})
export class CopieModule {}
