import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Competence } from '../competence/competence.entity';
import { Convocation } from '../convocation/convocation.entity';
import { Lot } from '../lot/lot.entity';

@Entity()
export class Copie {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: true,
  })
  note: number;

  @OneToOne(() => Competence)
  @JoinColumn()
  competence: Competence;

  @OneToOne(() => Convocation)
  @JoinColumn()
  convocation: Convocation;

  @OneToOne(() => Lot)
  @JoinColumn({ name: 'lotNumero' })
  lot: Lot;
}
