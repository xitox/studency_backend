import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Copie } from './copie.entity';

@Injectable()
export class CopieService {
  constructor(
    @InjectRepository(Copie)
    private copieRepository: Repository<Copie>,
  ) {}

  findAll(): Promise<Copie[]> {
    return this.copieRepository.find();
  }

  async findOne(id: number): Promise<Copie> {
    return await this.copieRepository.findOne({
      where: { id: id },
      relations: ['competence', 'convocation', 'lot', 'competence.domaine'],
    });
  }

  async remove(id: string): Promise<void> {
    await this.copieRepository.delete(id);
  }
}
