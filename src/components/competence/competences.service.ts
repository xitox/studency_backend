import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Competence } from './competence.entity';

@Injectable()
export class CompetenceService {
  constructor(
    @InjectRepository(Competence)
    private competenceRepository: Repository<Competence>,
  ) {}

  findAll(): Promise<Competence[]> {
    return this.competenceRepository.find();
  }

  findOne(id: string): Promise<Competence> {
    return this.competenceRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.competenceRepository.delete(id);
  }
}
