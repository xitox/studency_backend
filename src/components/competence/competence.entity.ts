import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Domaine } from '../domaine/domaine.entity';

@Entity()
export class Competence {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  nom: string;
  @ManyToOne(() => Domaine, (domaine) => domaine.competences)
  domaine: Domaine;
}
