import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CompetenceService } from './competences.service';
import { CompetenceController } from './competences.controller';
import { Competence } from './competence.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Competence])],
  providers: [CompetenceService],
  controllers: [CompetenceController],
})
export class CompetenceModule {}
