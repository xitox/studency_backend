import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VagueService } from './vague.service';
import { VagueController } from './vague.controller';
import { Vague } from './vague.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Vague])],
  providers: [VagueService],
  controllers: [VagueController],
})
export class VagueModule {}
