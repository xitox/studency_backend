import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Vague } from './vague.entity';

@Injectable()
export class VagueService {
  constructor(
    @InjectRepository(Vague)
    private vagueRepository: Repository<Vague>,
  ) {}

  findAll(): Promise<Vague[]> {
    return this.vagueRepository.find();
  }

  findOne(id: string): Promise<Vague> {
    return this.vagueRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.vagueRepository.delete(id);
  }
}
