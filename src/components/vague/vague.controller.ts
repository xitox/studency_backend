import { Controller, Get } from '@nestjs/common';

@Controller('vague')
export class VagueController {
  @Get()
  findAll(): string {
    return 'This action returns all cats';
  }
}
