import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Convocation } from '../convocation/convocation.entity';
import { Salle } from '../salle/salle.entity';

@Entity()
export class Vague {
  @PrimaryGeneratedColumn()
  numero: number;

  @Column()
  date: Date;

  @OneToMany(() => Convocation, (convocation) => convocation.vague)
  convocations: Convocation[];

  @ManyToMany(() => Salle)
  @JoinTable()
  salles: Salle[];
}
