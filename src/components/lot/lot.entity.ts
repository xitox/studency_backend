import {
  Entity,
  OneToOne,
  JoinColumn,
  PrimaryColumn,
  ManyToOne,
} from 'typeorm';
import { Correcteur } from '../correcteur/correcteur.entity';
import { Vague } from '../vague/vague.entity';

@Entity()
export class Lot {
  @PrimaryColumn({ type: 'int' })
  lotNumero: number;

  @ManyToOne(() => Correcteur, (correcteur) => correcteur.id)
  @JoinColumn()
  correcteur: Correcteur;

  @OneToOne(() => Vague)
  @JoinColumn()
  lot: Vague;
}
