import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Lot } from './lot.entity';

@Injectable()
export class LotService {
  constructor(
    @InjectRepository(Lot)
    private lotRepository: Repository<Lot>,
  ) {}

  findAll(): Promise<Lot[]> {
    return this.lotRepository.find();
  }

  findOne(id: string): Promise<Lot> {
    return this.lotRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.lotRepository.delete(id);
  }
}
