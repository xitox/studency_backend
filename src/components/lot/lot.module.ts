import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LotService } from './lot.service';
import { LotController } from './lot.controller';
import { Lot } from './lot.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Lot])],
  providers: [LotService],
  controllers: [LotController],
})
export class LotModule {}
