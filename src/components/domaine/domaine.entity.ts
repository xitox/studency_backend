import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Competence } from '../competence/competence.entity';

@Entity()
export class Domaine {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  nom: string;
  @Column()
  seuil: number;
  @Column()
  description: string;
  @OneToMany(() => Competence, (competence) => competence.domaine)
  competences: Competence[];
}
