import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DomaineService } from './domaine.service';
import { DomaineController } from './domaine.controller';
import { Domaine } from './domaine.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Domaine])],
  providers: [DomaineService],
  controllers: [DomaineController],
})
export class DomaineModule {}
