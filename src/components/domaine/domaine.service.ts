import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Domaine } from './domaine.entity';

@Injectable()
export class DomaineService {
  constructor(
    @InjectRepository(Domaine)
    private domaineRepository: Repository<Domaine>,
  ) {}

  findAll(): Promise<Domaine[]> {
    return this.domaineRepository.find();
  }

  findOne(id: string): Promise<Domaine> {
    return this.domaineRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.domaineRepository.delete(id);
  }
}
