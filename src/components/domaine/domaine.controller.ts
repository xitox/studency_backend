import { Controller, Get } from '@nestjs/common';
import { Domaine } from './domaine.entity';
import { DomaineService } from './domaine.service';

@Controller('domaine')
export class DomaineController {
  constructor(private service: DomaineService) {}

  @Get()
  async findAll(): Promise<Domaine[]> {
    return this.service.findAll();
  }
}
