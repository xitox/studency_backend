import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Groupe } from '../groupe/groupe.entity';

@Entity()
export class Etudiant {
  @PrimaryGeneratedColumn()
  numero: number;

  @Column()
  nom: string;

  @Column()
  prenom: string;

  @Column()
  password: string;

  @ManyToMany(() => Groupe)
  @JoinTable()
  groupes: Groupe[];
}
