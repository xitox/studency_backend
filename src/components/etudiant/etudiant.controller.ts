import { Body, Controller, HttpStatus, Post, Res } from '@nestjs/common';
import { EtudiantService } from './etudiant.service';
import * as bcrypt from 'bcrypt';
import { Response } from 'express';
import { EtudiantReq } from './interfaces/etudiantReq.dto';
import { EtudiantRes } from './interfaces/etudiantRes.dto';

@Controller('etudiant')
export class EtudiantController {
  constructor(private service: EtudiantService) {}

  @Post('login')
  async connect(@Body() etudiantReq: EtudiantReq, @Res() response: Response) {
    let userFound: any;
    try {
      userFound = await this.service.findOneByUserName(etudiantReq.numero);
    } catch (error) {
      response.status(HttpStatus.FORBIDDEN).send();
      return;
    }
    const hashedPassword = userFound.password;
    const numero = userFound.numero;
    bcrypt
      .compare(etudiantReq.password, hashedPassword)
      .then((validPassword) => {
        if (validPassword) {
          const user: EtudiantRes = { numero: numero };
          response.status(HttpStatus.ACCEPTED).send(user);
          return;
        } else {
          response.status(HttpStatus.FORBIDDEN).send();
          return;
        }
      })
      .catch((err) => console.log('error: ' + err));
  }
}
