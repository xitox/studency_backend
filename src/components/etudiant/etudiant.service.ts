import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Etudiant } from './etudiant.entity';

@Injectable()
export class EtudiantService {
  constructor(
    @InjectRepository(Etudiant)
    private etudiantRepository: Repository<Etudiant>,
  ) {}

  findAll(): Promise<Etudiant[]> {
    return this.etudiantRepository.find();
  }

  findOne(id: string): Promise<Etudiant> {
    return this.etudiantRepository.findOne(id);
  }

  findOneByUserName(numero: number): Promise<Etudiant> {
    return this.etudiantRepository.findOneOrFail({
      where: { numero: numero },
    });
  }

  async remove(id: string): Promise<void> {
    await this.etudiantRepository.delete(id);
  }
}
