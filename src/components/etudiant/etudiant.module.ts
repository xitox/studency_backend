import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EtudiantService } from './etudiant.service';
import { EtudiantController } from './etudiant.controller';
import { Etudiant } from './etudiant.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Etudiant])],
  providers: [EtudiantService],
  controllers: [EtudiantController],
})
export class EtudiantModule {}
