import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { DomaineModule } from './components/domaine/domaine.module';
import { CompetenceModule } from './components/competence/competences.module';
import { ConvocationModule } from './components/convocation/convocation.module';
import { CopieModule } from './components/copie/copie.module';
import { EtudiantModule } from './components/etudiant/etudiant.module';
import { GroupeModule } from './components/groupe/groupe.module';
import { LotModule } from './components/lot/lot.module';
import { SalleModule } from './components/salle/salle.module';
import { VagueModule } from './components/vague/vague.module';
import { CorrecteurModule } from './components/correcteur/correcteur.module';
import { ImportCsvModule } from './import-csv/import-csv.module';
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    DomaineModule,
    CompetenceModule,
    ConvocationModule,
    CopieModule,
    EtudiantModule,
    GroupeModule,
    LotModule,
    SalleModule,
    VagueModule,
    CorrecteurModule,
    ImportCsvModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
